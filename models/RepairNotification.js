const mongoose = require('mongoose')

const repairSchema = new mongoose.Schema({
  username: String,
  user_name: String,
  day: { type: Date, default: Date.now },
  class: Number,
  room: {
    type: 'ObjectId',
    ref: 'Building'
  },
  type: String,
  mach: String,
  device: {
    type: 'ObjectId',
    ref: 'Device'
  },
  staff_name: String,
  status: {
    type: 'ObjectId',
    ref: 'State'
  },
  change_day: { type: Date, default: Date.now },
  picture: String,
  cause: String
})

module.exports = mongoose.model('repairNotofication', repairSchema)