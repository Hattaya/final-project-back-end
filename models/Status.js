const mongoose = require('mongoose')

const statusSchema = new mongoose.Schema({
  name: String,
  usability: String
})

module.exports = mongoose.model('State', statusSchema)
