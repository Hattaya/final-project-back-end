const mongoose = require('mongoose')

const deviceSchema = new mongoose.Schema({
  name: String,
  type: String,
  checked: [String],
  usability: String
})

module.exports = mongoose.model('Device', deviceSchema)
