const mongoose = require('mongoose')

const buildSchema = new mongoose.Schema({
  room: String,
  class: Number,
  checked: [String],
  mach: [String],
  usability: String
})

module.exports = mongoose.model('Building', buildSchema)
