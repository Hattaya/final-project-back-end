const mongoose = require('mongoose')

const personSchema = new mongoose.Schema({
  username: String,
  password: String,
  name: {
    gender :String,
    first : String,
    last : String
  },
  tell: String,
  email: String,
  usability: String,
  Person_type: String
})

module.exports = mongoose.model('Person', personSchema)
