const express = require('express')
var cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const staffsRouter = require('./routes/staffs')
const statusRouter = require('./routes/status')
const devicesRouter = require('./routes/devices')
const buildingRouter = require('./routes/building')
const repairsRouter = require('./routes/repair')



const app = express()

const mongoose = require('mongoose')
mongoose.connect('mongodb://admin:informatics@localhost/testdb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connnection error: '))
db.once('open', function () {
  console.log('connect database : testdb')
})

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/staffs', staffsRouter)
app.use('/statuses', statusRouter)
app.use('/devices', devicesRouter)
app.use('/building', buildingRouter)
app.use('/repairNotofication', repairsRouter)

module.exports = app
