const Status = require('../models/Status')
const statusController = {
  statusList: [],
  async addStatus(req, res, next) {
    const payload = req.body
    const state = new Status(payload)
    try {
      await state.save()
      res.json(state)
    } catch (err) {
      res.status(500).send(err)
    }

  },
  async updateStatus(req, res, next) {
    const payload = req.body
    try {
      const state = await Status.updateOne({ _id: payload._id }, payload)
      res.json(state)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteStatus(req, res, next) {
    const { id } = req.params
    try {
      const state = await Status.deleteOne({ _id: id })
      res.json(state)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getStatuses(req, res, next) {
    try {
      const state = await Status.find({})
      res.json(state)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getStatus(req, res, next){
    const { id } = req.params
    try {
      const state = await Status.findById(id)
      res.json(state)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = statusController
