const Build = require('../models/Build')
const buildController = {
  buildList: [],
  async addBuild(req, res, next) {
    const payload = req.body
    const build = new Build(payload)
    try {
      await build.save()
      res.json(build)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateBuild(req, res, next) {
    const payload = req.body
    try {
      const build = await Build.updateOne({ _id: payload._id }, payload)
      res.json(build)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteBuild(req, res, next) {
    const { id } = req.params
    try {
      const build = await Build.deleteOne({ _id: id })
      res.json(build)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getBuilds(req, res, next) {
    try {
      const build = await Build.find({})
      res.json(build)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getBuild(req, res, next) {
    const { id } = req.params
    try {
      const builds = await Build.findById(id)
      res.json(builds)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = buildController
