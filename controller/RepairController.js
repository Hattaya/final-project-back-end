const Repair = require('../models/RepairNotification')
const repairController = {
  repairList: [],
  async addRepair(req, res, next) {
    const payload = req.body
    const repair = new Repair(payload)
    try {
      await repair.save()
      res.json(repair)
    } catch (err) {
      res.status(500).send(err)
    }

  },
  async updateRepair(req, res, next) {
    const payload = req.body
    console.log(payload)
    try {
      const repair = await Repair.updateOne({ _id: payload._id }, payload)
      res.json(repair)
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  },
  async deleteRepair(req, res, next) {
    const { id } = req.params
    try {
      const repair = await Repair.deleteOne({ _id: id })
      res.json(repair)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRepairs(req, res, next) {
    try {
      const repair = await Repair.find({}).populate('room').populate('device').populate('status')
      res.json(repair)
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  },
  async getRepair(req, res, next){
    const { id } = req.params
    try {
      const repair = await Repair.findById(id)
      res.json(repair)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = repairController
