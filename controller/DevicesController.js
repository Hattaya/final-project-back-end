const Device = require('../models/Device')
const devicesController = {
  devicesList: [],
  async addDevices (req, res, next) {
    const payload = req.body
    const device = new Device(payload)
    try {
      await device.save()
      res.json(device)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateDevices (req, res, next) {
    const payload = req.body
    try {
      const device = await Device.updateOne({ _id: payload._id }, payload)
      res.json(device)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteDevices (req, res, next) {
    const { id } = req.params
    try {
      const device = await Device.deleteOne({ _id: id })
      res.json(device)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDevices(req, res, next) {
  try {
    const device = await Device.find({})
    res.json(device)
  } catch (err) {
    res.status(500).send(err)
  }
},
async getDevice (req, res, next){
  const { id } = req.params
  try {
    const devices = await Device.findById(id)
    res.json(devices)
  } catch (err) {
    res.status(500).send(err)
  }
}
}
module.exports = devicesController
