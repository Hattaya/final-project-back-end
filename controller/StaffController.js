const Person = require('../models/Person')
const personController = {
  personList: [
  ],
  lastId: 3,
  async addPerson(req, res, next) {
    const payload = req.body
    console.log(payload)
    const person = new Person(payload)
    try {
      await person.save()
      res.json(person)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePerson(req, res, next) {
    const payload = req.body
    try {
      const person = await Person.updateOne({ _id: payload._id }, payload)
      res.json(person)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePerson(req, res, next) {
    const { id } = req.params
    try {
      const person = await Person.deleteOne({ _id: id })
      res.json(person)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPersons(req, res, next) {
    try {
      const person = await Person.find({})
      res.json(person)
      console.log(person)
    } catch (err) {
      res.status(500).send(err)
    }

  },
  async getPerson(req, res, next) {
    const { id } = req.params
    try {
      const person = await Person.findById(id)
      res.json(person)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsername(req, res, next) {
    const { username } = req.params
    console.log(req.params.username)
    try {
      const person = await Person.findOne({username: username})
      res.json(person)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async login(req, res, next) {
    const { username,password } = req.body
    console.log(req.params.username)
    try {
      const person = await Person.findOne({username: username, password: password})
      res.json(person)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = personController
