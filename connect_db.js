const mongoose = require('mongoose')

mongoose.connect('mongodb://admin:informatics@localhost/testdb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error :'))
db.once('open', () => {
  console.log('connect database')
})

const repairSchema = new mongoose.Schema({
  username: String,
  name: {
    gender :String,
    first : String,
    last : String
  },
  day: { type: Date, default: Date.now },
  room: String,
  mach: String,
  device: String,
  staff_name: String,
  status: String,
  change_day: { type: Date, default: Date.now },

})

const repair = mongoose.model('repairNotofication', repairSchema)

const building = new repair({
  username: "60160148",
  name: {
    gender :"นางสาว",
    first : "หัทยา",
    last : "นาคชูแก้ว"
  },
  day: Date.now() ,
  room: "IF-3C01",
  mach: "C1",
  device: "เมาส์",
  staff_name: "นายสิทธิพงษ์ ฉิมไทย",
  status: "แจ้งซ่อม",
  change_day: Date.now(),
})

building.save((err, states) => {
  if (err) return console.error(err);
})