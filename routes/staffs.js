const express = require('express')
const router = express.Router()
const staffController = require('../controller/StaffController')

router.post('/login', staffController.login)

router.get('/', staffController.getPersons)

router.get('/:id', staffController.getPerson)

router.post('/', staffController.addPerson)

router.put('/', staffController.updatePerson)

router.delete('/:id', staffController.deletePerson)

module.exports = router
