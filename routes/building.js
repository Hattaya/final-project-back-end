const express = require('express')
const router = express.Router()
const buildController = require('../controller/BuildingController')

/* GET users listing. */
router.get('/', buildController.getBuilds)

router.get('/:id', buildController.getBuild)

router.post('/', buildController.addBuild)

router.put('/', buildController.updateBuild)

router.delete('/:id', buildController.deleteBuild)

module.exports = router
