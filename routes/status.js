const express = require('express')
const router = express.Router()
const statusController = require('../controller/StatusController')

/* GET users listing. */
router.get('/', statusController.getStatuses)

router.get('/:id', statusController.getStatus)

router.post('/', statusController.addStatus)

router.put('/', statusController.updateStatus)

router.delete('/:id', statusController.deleteStatus)

module.exports = router
