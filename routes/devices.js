const express = require('express')
const router = express.Router()
const devicesController = require('../controller/DevicesController')


/* GET users listing. */
router.get('/', devicesController.getDevices)

router.get('/:id', devicesController.getDevice)

router.post('/', devicesController.addDevices)

router.put('/', devicesController.updateDevices )

router.delete('/:id', devicesController.deleteDevices )

module.exports = router
